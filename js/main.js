$(document).ready(function() {

	var data = {};

	function generateCar(obj) {
		$.each(data, function(index,value) {
	    	var thumbnail = '<div class="catalog-img" style="background-image: url('+value.thumbnail+')"></div>';
	    	var title = '<h3 class="catalog-item">'+value.name+'</h3>'; 
	    	var location = '<p class="catalog-listing-item-location"><i class="fa fa-map-marker"></i><span>'+value.location+'</span></p>';
	    	var price = '<h4 class="catalog-item-price">'+value.price+'</h4>';
	    	var desc = '<p 	class="catalog-description">'+value.description+'</p>';
	    	var template = '<div class="catalog-listing">' + thumbnail + title + location + price + desc + '</div>';
	    	$('.catalog').append(template);
	    });
	}

	$.getJSON("cars.json", function(json) {
		data = json.cars;
		generateCar(data);
	});
});

$(document).ready(function(){
	var btt = $('.back-to-top');

	btt.on('.click', function(e){
		$('html,body').animate({
			scrollTop:0
		},500);
		e.preventDefault();
	});

	$(window).on('scroll', function(){
		var self = $(this),
			height = self.height(),
			tofp =self.scrollTop();

		if (top > height){
			if(!btt.is(':visible')){
				btt.show();
			}
		}else{
			btt.show();
		}	

	});

});

